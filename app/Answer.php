<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded =[];

    public static function boot() {
        parent::boot();

        static::created(function($answer) {
            $answer->question->increment('answers_count');
        });
        static::deleted(function($answer){
            $answer->question->decrement('answers_count');
        });
    }
    public function getAvatarAttribute() {
        $size = 40;
        $name = $this->name;
        return "https://ui-avatars.com/api/?name={$name}&rounded=true&size={$size}";
    }
    public function getCreatedDateAttribute() {
        return $this->created_at->diffForHumans();
    }
    public function getBestAnswerStatusAttribute(){
        if($this->id === $this->question->best_answer_id) {
            return "text-success";
        }
        return "text-dark";
    }
    public function getIsBestAttribute(){
        return $this->id === $this->question->best_answer_id;
    }
    /*
     * RELATIOSHIP METHODS
     */
    public function question() {
        return $this->belongsTo(Question::class);
    }
    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function votes() {
        return $this->morphToMany(User::class,'vote')->withTimestamps();
    }

    
    public function vote(int $vote) {
        $this->votes()->attach(auth()->id(),['vote'=>$vote]);
        if($vote < 0) 
        {
            $this->decrement('votes_count');
        }
        else
        {
            $this->increment('votes_count');
        }
    }
    public function updateVote(int $vote) 
    {
        //user may already up-voted this question and now downvotes (vote_count = 10) an dnow user and now user down votes (vote_count =8)
        $this->votes()->updateExistingPivot(auth()->id(),['vote'=>$vote]);
        if($vote < 0) 
        {
            $this->decrement('votes_count');
            $this->decrement('votes_count');
        }
        else 
        {
            $this->increment('votes_count');
            $this->increment('votes_count');
        }
    } 
}