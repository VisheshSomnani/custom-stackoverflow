<?php

namespace App\Http\Controllers;

use App\Question;

use Illuminate\Http\Request;
use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;

use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware(['auth'])->only(['create','store','edit','update']);
    }
    public function index()
    {
        $questions = Question::with('owner')
                                ->latest()
                                ->paginate(10); //eager load
        return view('questions.index',compact([
            'questions'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        app('debugbar')->disable();
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
       
        auth()->user()->questions()->create([
            'title' =>$request->title,
            'body' =>$request->body
        ]);

        session()->flash('success','Question has been added succesfully!');
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    /**
     * In below function we are taking question as an argument but we are giving slug from getUrlAttribute so we had bind the slug with question object in RouteServiceProvider
     */
    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show',compact([
            'question'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        if($this->authorize('update',$question)) {
            app('debugbar')->disable();
            return view('questions.edit',compact([
                'question'
            ]));
        }
        abort(403);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update',$question)) {
            $question->update([
                'title' =>$request->title,
                'body' =>$request->body
            ]);
            session()->flash('success','Question has been Updated succesfully!');
            return redirect(route('questions.index'));
        }   
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        if($this->authorize('delete',$question)) {
            $question->delete();
            session()->flash('success','Question has been deleted Sucessfully!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
